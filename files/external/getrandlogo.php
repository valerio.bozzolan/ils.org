<?php

if (rand() % 3 == 0) {
    $path = '../images/associazioni/*.png';
}
else {
    $path = '../images/sponsor/*.png';
}

$f = glob($path);

$tot = count($f) - 1;
$a = $f[rand(0, $tot)];

if ( file_exists($a) ) {
    header('Content-Type: image/png');
    echo file_get_contents ($a);
} else {
    echo "File not found at " . $path;
}
