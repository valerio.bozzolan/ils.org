<!--
.. title: MERGE-it 2021
.. slug: mergeit-2021
.. date: 2021-05-14 00:00:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
.. image_copy: 
.. previewimage: /images/posts/mergeit.png
-->


MERGE-it, la maggiore conferenza italiana dedicata alle libertà digitali, torna sabato 22 maggio dalle 10:00 alle 18:30, in modalità online.

L'accesso è gratuito e non è necessario registrarsi. Collegandosi al sito [MERGE-IT.net](https://merge-it.net/) si potrà partecipare comodamente dal proprio computer o smartphone ed interagire in tempo reale con i relatori sui temi di sviluppo software, open data, libertà digitali, sostenibilità, privacy e conoscenza libera, con approfondimenti relativi all'industria, alla scuola ed alla pubblica amministrazione.

<!-- TEASER_END -->

La prima edizione del MERGE-it si è svolta nel 2018, a Torino ed ha ospitato le comunità italiane del software libero e open source. L'edizione successiva, prevista nel 2020, è stata annullata a causa della pandemia di COVID-19. Destino diverso ha avuto il [Linux Day 2020](https://www.ils.org/2020/05/23/linux-day-2020.html), svolto online insieme ad itWikiCon - la conferenza nazionale dei progetti Wikipedia: il successo è stato tale da spingere i volontari a promuovere ulteriori incontri in presenza remota, consolidando un gruppo inter-comunitario che ha preso le redini della nuova edizione di MERGE-it.

Numerose sono le realtà nazionali, associazioni e community che si sono attivate per rappresentare il "sistema operativo" del Paese, tra cui:

- Italian Linux Society -  dal 1994 a sostegno del software libero in Italia
- GARR - dal 1991, il consorzio no-profit di banda ultralarga per l'istruzione, l'università e la ricerca
- Wikimedia Italia - l'associazione per la diffusione della conoscenza libera, che sostiene Wikipedia, i progetti Wikimedia e OpenStreetMap
- Developers Italia - gruppo legato alla trasformazione digitale dell'Italia nella pubblica amministrazione
- Free Software Foundation Europe - dal 2001 attivi per riavere il controllo della tecnologia
- Informatici Senza Frontiere - usi intelligenti, sostenibili e solidali della tecnologia
- LibreItalia - la community LibreOffice, la suite di produttività individuale e aziendale
- Mozilla Italia - gli italiani impegnati nell'evoluzione del web, tutelando i suoi internauti
- OpenStreetMap Italia - i contributori della "Wikipedia delle mappe", la raccolta mondiale di dati geografici liberi
- onData - verso l'apertura dei dati pubblici, realmente accessibili a tutti
- Python Italia - community con più di 400 membri sull'omonimo linguaggio di programmazione
- Rust Italia - i promotori del linguaggio di programmazione sviluppato da Mozilla Research
- GFOSS.it - a tutela di un'informazione geografica globale libera
- Lavagna Libera - docenti e tecnici di riferimento per software e contenuti liberi nella scuola
- Scala Italy - la community sull'omonimo linguaggio di programmazione funzionale e ad oggetti

La mattina sarà dedicata ad una breve presentazione di tutte le associazioni e community aderenti, ed è prevista la possibilità di interagire via chat in appositi canali tematici per conoscersi meglio ed avanzare domande.

Nel pomeriggio si svolgeranno sei diverse tavole rotonde tematiche, che vedranno i diversi soggetti coinvolti confrontarsi e misurarsi su temi di attualità.

Al termine della conferenza tutte le registrazioni audio/video degli interventi saranno pubblicate in licenza libera per incoraggiarne lo studio, la modifica e la condivisione.