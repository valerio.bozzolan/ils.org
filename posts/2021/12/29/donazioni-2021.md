<!--
.. title: Donazioni 2021
.. slug: donazioni-2021
.. date: 2021-12-29 00:00:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
.. image_copy: Photo by <a href="https://unsplash.com/@roblaughter?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Rob Laughter</a> on <a href="https://unsplash.com/s/photos/present?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Unsplash</a>
.. previewimage: /images/posts/donazioni2021.jpg
-->


Per concludere l'anno 2021, Italian Linux Society ha distribuito oltre 7000 euro in donazioni a progetti sul software libero, l'Open Source e iniziative legate alla cultura libera.

<!-- TEASER_END -->

* [Framasoft](https://framasoft.org/it/): 1000 euro. L'organizzazione francese che implementa alcune tra le più importanti piattaforme libere online, tra cui PeerTube (di cui ILS hosta [una istanza](https://video.linux.it/videos/trending), dove si trovano i video del [Linux Day Online](https://www.linuxday.it/2021/) e non solo) e Mobilizon
* [Cryptpad](https://cryptpad.fr/): 1000 euro. Una collezione di applicazioni per la collaborazione online, particolarmente apprezzata dagli insegnanti soprattutto in periodo di Didattica a Distanza. Il progetto OpenDidattica, supportato da ILS, ne hosta [una istanza](https://cryptpad.opendidattica.org/) pubblicamente accessibile
* [Minetest](https://www.minetest.net/): 1000 euro. Videogioco open source in cui immergersi in un mondo virtuale per costruire tutto ciò che si vuole, assemblando "mattoncini" di materiali diversi. Non solo divertente, ma anche adoperato a fini ludici e didattici da diversi docenti
* [PHP Foundation](https://opencollective.com/phpfoundation): 1000 dollari. Gran parte dei progetti ILS sono realizzati in PHP, uno dei linguaggi di programmazione più popolari in assoluto per il web. A tal proposito: si trovano (ovviamente in licenza libera e open source) nei nostri repository su [GitHub](https://github.com/ItalianLinuxSociety/) e [GitLab](https://gitlab.com/ItalianLinuxSociety/), qualora qualcuno volesse darci una mano con lo sviluppo e la manutenzione
* [Globaleaks](https://www.globaleaks.org/): 500 euro. Piattaforma (realizzata in Italia) per il "whistleblowing", ovvero la condivisione anonima di informazioni sensibili e delicate. Utilizzata in particolare presso diverse amministrazioni pubbliche per la segnalazione degli illeciti e la prevenzione della corruzione
* [Espanso](https://espanso.org/): 500 euro. Piccola applicazione freesoftware forse meno nota ai più, ma molto assai utilizzata ed apprezzata. Questa donazione in particolare è motivata dalla volontà di sostenere il software libero Made in Italy, e premiare gli sviluppatori che vivono nel nostro Paese e contribuiscono ad accrescere il patrimonio libero
* [Vikidia](https://it.vikidia.org/wiki/Pagina_principale): 500 euro. L'enciclopedia online per i più piccoli, le cui voci sono realizzate da (e per) bambini e bambine dagli 8 anni. Utile a scuola, sia per attingere ai contenuti (espressi in un linguaggio più facile ed accessibile rispetto alla Wikipedia che tutti amiamo) che per coinvolgere i ragazzi nello scrivere i propri
* [Devol.it](https://devol.it/): 500 euro. Il collettivo che mantiene diverse istanze dei più popolari social network liberi, estensione italiana del cosiddetto "fediverso" decentralizzato e privacy-friendly, tra cui l'installazione Mastodon su cui si trova l'[account di Italian Linux Society](https://mastodon.uno/@ItaLinuxSociety/)
* [onData](https://ondata.it/): 500 euro. La principale associazione italiana che si occupa di open data, sia degli aspettivi politici (la trasparenza delle istituzioni e l'accessibilità alle informazioni) che di quelli tecnici (formati, software di elaborazione e visualizzazioni). Nel corso del 2021, ILS ha aderito alla sua campagna ["Dati Bene Comune"](https://www.datibenecomune.it/)
* [giua@school](https://github.com/trinko/giuaschool): 500 euro. Il nuovo registro elettronico open source per le scuole italiane, nato da LAMPSchool, sviluppato dagli insegnanti e al di fuori delle classiche logiche proprietarie dei fornitori più popolari
* [xFiles](https://xfiles.noads.it/): 500 euro. Una raccolta di filtri e configurazione per bloccare sul proprio browser i contenuti traccianti in cui si incappa navigando il web, e dunque uno strumento (estremamente consigliato!) per reagire attivamente alle quotidiane invasioni della nostra privacy

Grazie a chi ogni anno contribuisce a [sostenere](/sostieni/) Italian Linux Society, versando la propria quota di [iscrizione](https://ilsmanager.linux.it/ng/register) o con una donazione o con una [sponsorizzazione](/sponsorizzazioni/), e ci aiuta ad aiutare la community.

Infine, l'occasione si presta per invitare nuovamente - come oramai ogni fine d'anno - tutti gli operatori professionali del settore IT che lavorano in modo più o meno diretto con software libero e open source - i quali, impossibile nasconderlo, sono una vasta maggioranza - ad aderire alla [Campagna 1%](https://www.linux.it/unopercento/) e devolvere una parte del proprio fatturato appunto ai progetti ed ai componenti software che adoperano per svolgere le proprie attività.