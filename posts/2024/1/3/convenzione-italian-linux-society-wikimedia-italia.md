<!--
.. title: Nuova convenzione fra Italian Linux Society e Wikimedia Italia
.. slug: convenzione-italian-linux-society-wikimedia-italia
.. date: 2024-03-11 00:00:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
.. image_copy: <a rel="nofollow" href="https://commons.wikimedia.org/wiki/File:Bratislava_New_Year_Fireworks.jpg">Ondrejk, Public domain, via Wikimedia Commons</a>
.. previewimage: /images/posts/ils_plus_wmit.png
-->

Italian Linux Society ha firmato la nuova convenzione con Wikimedia Italia che varrà dall'11 marzo 2024 fino al 2027. Cosa c'è di nuovo?

<!-- TEASER_END -->

Il [testo completo della nuova convenzione fra ILS e WMIT](https://wiki.wikimedia.it/wiki/Convenzioni/2023/convenzione_Italian_Linux_society_-_WMI) prevede nuovi impegni per entrambe le organizzazioni. Vediamoli.

## Nuovi impegni di Italian Linux Society

Italian Linux Society si impegnerà ancora di più nel diffondere gli strumenti e le licenze Creative Commons. Limitatamente a quelle libere ovviamente, ovvero queste tre:

* [Creative Commons Zero](https://creativecommons.org/publicdomain/zero/1.0/deed.it)
* [Creative Commons BY](https://creativecommons.org/licenses/by/4.0/deed.it)
* [Creative Commons BY-SA](https://creativecommons.org/licenses/by-sa/4.0/deed.it)

Inoltre, le competenze tecniche di Italian Linux Society rimarranno a disposizione di Wikimedia Italia nel caso WMIT abbia bisogno di pareri sull'adozione di software. Soprattutto per ricevere pareri su come sostituire software proprietario con software libero. Ottimo per tutti.

Italian Linux Society si impegnerà anche nel divulgare maggiormente ai propri soci l'esistenza degli strumenti di Wikimedia Italia, fra cui lo strumento [Microgrant di Wikimedia Italia](https://wiki.wikimedia.it/wiki/Microgrant) che permette di richiedere fondi fino a 1000€ per un nuovo progetto o un evento.

A proposito, ringraziamo ancora la commissione microgrant che revisiona e abilita progetti molto concreti!

## Alcuni Microgrant approvati da Wikimedia Italia recentemente

Avete bisogno di una bozza di microgrant? Chiedete alla squadra di Zughy che voleva portare il software libero [Minetest.net](https://www.minetest.net/) alla conferenza FOSDEM 2024 insieme al suo gruppo, e ci sono riusciti:

<https://wiki.wikimedia.it/wiki/Microgrant/2024/Minetest_e_Lua_al_FOSDEM_2024>

Sul banchetto di Minetest al FOSDEM seguirà un riepilogo più dettagliato molto presto.

Ringraziamo anche l'utente Ferdi2005 che sta recentemente organizzando un bellissimo incontro della community tecnica a Catania, anche grazie ad una richiesta Microgrant:

<https://forum.linux.it/t/wikimedia-hackathon-satellite-event-a-catania-aprile-2024/1267>

Un grazie anche alla community del [Pordenone user group](https://www.pnlug.it/) che ha ottenuto un microgrant, giusto lo scorso Linux Day. Hanno organizzato un bellissimo evento in un teatro:

<https://wiki.wikimedia.it/wiki/Microgrant/2023/linuxday_2023_PNlug:_%22Internet_libero,_sicuro%3F%22>

Quindi, se la tua community desidera presentare un microgrant, per esempio per un Linux Day inerente ai temi Wikimedia, sbircia quelli già fatti e leggi bene il regolamento:

<https://wiki.wikimedia.it/wiki/Microgrant>

## Nuovi impegni di Wikimedia Italia

Nella nuova convenzione, Wikimedia Italia si impegna maggiormente a patrocinare la conferenza Linux Day (che quest'anno si terrà il <b>28 ottobre 2024</b>) comprendendo altresì i singoli eventi nelle città ufficialmente menzionate sul sito web ufficiale. Il patrocinio delle singole città è praticamente automatico! Ottimo per far risparmiare tempo a tutti.

Inoltre, Wikimedia Italia si impegna a coinvolgere maggiormente i suoi soci per le questioni legati al software, in modo da aumentare la consapevolezza interna su questi temi critici, non secondari ai contenuti.

## Ricapitolando

Per le persone più curiose qui si trova il testo integrale della nuova convenzione:

<https://wiki.wikimedia.it/wiki/Convenzioni/2023/convenzione_Italian_Linux_society_-_WMI>

Una scusa in più per unire le forze. In questo modo possiamo affrontare meglio le sfide e gli ostacoli del software libero e dei contenuti liberi.

## Crediti fotografici

L'[immagine di Wikimedia Italia + Italian Linux Society](/images/posts/ils_plus_wmit.png) è rilasciata in licenza libera [Creative Commons BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/) dall'utente Valerio Bozzolan usando il software libero di fotoritocco [GIMP](https://it.wikipedia.org/wiki/GIMP).

Queste sono le altre immagini libere che la compongono con i relativi crediti:

* [Adesivi e spille Linux.it dal Linux Day 2022 in Italia.jpg](https://commons.wikimedia.org/wiki/File:Stickers_and_pins_love_Linux.it_from_Linux_Day_2022_in_Italy.jpg)
* [Portachiavi di Wikidata.jpg](https://commons.wikimedia.org/wiki/File:Wikidata_Key_chains.jpg)
* [Logo di Italian Linux Society con testo](https://commons.wikimedia.org/wiki/File:Italian_Linux_Society_logo_text_penguin_circle_black.svg)
* [Logo di Wikimedia Italia con testo](https://commons.wikimedia.org/wiki/File:Wikimedia_Italia-logo-black.svg)

Lunga vita a Italian Linux Society, Wikimedia Italia e a tutte le community legate al software libero e ai contenuti liberi.

## Alla prossima assemblea!

Si avvicinano anche le relative assemblee di entrambe le organizzazioni.

La prossima <b>assemblea di Italian Linux Society</b> sarà in seconda convocazione di <b>sabato 6 aprile 2024</b> alle ore <b>15:00</b>. Sei ancora in tempo per partecipare o delegare:

<https://ilsmanager.linux.it/ng/assembly>
