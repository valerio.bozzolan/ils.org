<!--
.. title: La Libertà non ha Prezzo: Progetto LampSchool
.. slug: la-libert-non-ha-prezzo-progetto-lampschool
.. date: 2015-10-19 00:00:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
.. image_copy: 
.. previewimage:
-->

Dopo l'inatteso successo della <a href="{% link _posts/2015-07-02-la-libert-non-ha-prezzo.md %}">prima raccolta fondi</a> condotta su <a href="http://donazioni.linux.it/">donazioni.linux.it</a>, che ha permesso di raccogliere un totale di <a href="http://donazioni.linux.it/progetti/">5538 euro in favore del progetto Lightboard</a>, annunciamo oggi la seconda iniziativa di crowdfunding per il software libero.

Questa volta protagonista della campagna è un progetto già esistente ed in uso presso numerose scuole: <a rel="nofollow" href="http://www.lampschool.it/">LampSchool</a>, sviluppato e mantenuto grazie all'impegno della rete DematVR di Verona. Il programma implementa il registro elettronico di classe, tipologia di applicazione la cui adozione è stata resa obbligatoria nelle scuole pochi anni fa da parte del Ministero dell'Istruzione per tenere traccia digitale di voti, assenze ed orari; contrariamente alla pletora di soluzioni proprietarie (e spesso molto costose) sbocciate a seguito di tale direttiva, LampSchool è rilasciato con una licenza libera (AGPLv3), <a rel="nofollow" href="http://sourceforge.net/projects/lampschool/">il suo repository è pubblicamente accessibile</a>, ed è distribuito gratuitamente a beneficio di tutte le scuole che lo vogliano utilizzare.

Benché LampSchool già integri pressoché tutte le funzioni necessarie, la sua più grande limitazione è la mancanza di integrazione col protocollo del sistema "SIDI", il portale centralizzato attraverso cui il Ministero aggrega ed elabora le informazioni provenienti da tutte le scuole presenti sul territorio. Questa campagna è pertanto specificatamente rivolta a colmare tale lacuna, finanziando lo sviluppo di una porzione di codice che implementi il protocollo, permetta anche a LampSchool di interfacciarsi in modo automatico con il portale SIDI, e possa essere all'occorrenza riutilizzata all'interno di applicativi simili.

Ovvero: fare quello che la <a rel="nofollow" href="http://hubmiur.pubblica.istruzione.it/web/istruzione/dg-sssi/sviluppo">Direzione dei Sistemi Informativi del Ministero dell'Istruzione</a> avrebbe dovuto fare tempo addietro, per agevolare e facilitare l'adozione del sistema di centralizzazione ed aggregazione digitale, ma, ahinoi, non ha mai fatto. Anche in questa occasione la nostra raccolta fondi, spontaneamente sostenuta dai cittadini, vuole essere in primis un richiamo per le istituzioni ed un invito a rivedere l'approccio al processo di digitalizzazione della pubblica amministrazione, presso cui l'adozione di licenze libere e di modelli di sviluppo opensource permetterebbe non solo trasparenza ed indipendenza ma anche forti risparmi economici e rapidi tempi di integrazione.

Anche per il progetto LampSchool i soci di Italian Linux Society mettono a disposizione i primi 3000 euro, e tutte le quote delle nuove <a href="/iscrizione">iscrizioni all'associazione</a> che arriveranno nel corso della campagna saranno immediatamente destinate a questo fondo.

Sostieni anche tu il software libero con una donazione, anche da pochi euro. <a href="http://donazioni.linux.it/">Con l'aiuto di tutti, facciamo il software di tutti</a>.