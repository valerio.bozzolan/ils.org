<!--
.. title: Linux Day 2013
.. slug: linux-day-2013
.. date: 2013-11-01 00:00:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
.. image_copy: 
.. previewimage:
-->

Sabato scorso si e' svolta la <a href="https://www.linuxday.it/13/">tredicesima edizione del Linux Day</a>, che una volta di piu' si conferma essere la principale manifestazione italiana di promozione, informazione e divulgazione sui temi del software libero e, in modo trasversale, dei diritti digitali e della condivisione dei saperi.

Ad una settimana dall'evento vediamo spuntare sui siti web dei 107 gruppi organizzatori che hanno aderito quest'anno foto, slides, video, ma soprattutto tanti commenti positivi e tanti ringraziamenti. Cui molto volentieri ci uniamo, rivolgendoci naturalmente al nutrito pubblico di persone interessate - sia esperti che curiosi - ma ancora prima ai volontari, che a centinaia ogni anno si mobilitano in massa per allestire il miglior evento possibile. Investendo tempo, pazienza e denaro per comunicare e supportare un messaggio, quello della tecnologia come bene comune.

Nelle prossime settimane verra' erogato l'ormai consueto questionario atto a misurare le performance di quest'anno, in termini di affluenza e organizzazione interna, i cui risultati saranno pubblicati prossimamente.

L'appuntamento col Linux Day torna ad ottobre 2014, e nel frattempo invitiamo tutti gli interessati a <a href="https://lugmap.linux.it/">cercare e contattare il gruppo di appassionati</a> piu' vicino a casa o <a rel="nofollow" href="https://lugmap.linux.it/eventi">il prossimo appuntamento</a> che si svolgera' nella propria zona.