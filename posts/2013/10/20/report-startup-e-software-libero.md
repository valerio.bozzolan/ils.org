<!--
.. title: Report: Startup e Software Libero
.. slug: report-startup-e-software-libero
.. date: 2013-10-20 00:00:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
.. image_copy: 
.. previewimage:
-->

Finalmente pronti i risultati dell'indagine "Innovazione Libera!", iniziativa volta a misurare il grado di penetrazione del software libero presso le startup italiane <a href="{% link _posts/2013-07-16-innovazione-libera.md %}">annunciata in luglio</a> e per la quale sono stati spediti svariati inviti scaglionati prima e dopo l'estate.

Dalle risposte date alle poche e semplici domande proposte nel questionario emergono sia informazioni già note e consolidate (quali, ad esempio, il predominio di Linux sui server che erogano servizi sull'Internet) che dati inediti ed inattesi, come la percentuale di coinvolgimento delle giovani imprese nella community di sviluppo opensource.

La panoramica fornita suggerisce come il freesoftware sia ampiamente conosciuto e adottato presso le stelle nascenti dell'imprenditoria nostrana ed in taluni casi rappresenti una scelta primaria che non una alternativa di second'ordine. Per i costi di accesso, ma anche per le caratteristiche esclusive del software libero: l'assenza di vincoli nell'utilizzo e la possibilità di intervenire sul codice sorgente per modificarlo a proprio piacimento e secondo le proprie esigenze.

Il report completo puo' essere scaricato <a href="/pdf/InnovazioneLibera_2013.pdf">qui in formato PDF</a>.

Grazie a tutte le realtà che hanno collaborato.