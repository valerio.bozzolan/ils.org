<!--
.. title: Aspettando il Linux Day 2013
.. slug: aspettando-il-linux-day-2013
.. date: 2013-06-21 00:00:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
.. image_copy: 
.. previewimage:
-->

<p>
Italian Linux Society (ILS) ha fissato la data del Linux Day 2013 per sabato 26 ottobre, e grazie al contributo di <a rel="nofollow" href="http://www.lpi-italia.org/">Linux Professional Institute</a> ha avviato la macchina organizzativa che anche nel 2013 porterà la comunità del software libero a ritrovarsi in ogni parte d'Italia.
</p>
<p>
Il tema del <a href="https://www.linuxday.it/">Linux Day 2013</a> sarà l'innovazione, un fattore che da sempre contraddistingue il software libero ma che non è sufficientemente riconosciuto al di fuori della comunità. Per esempio sono in pochi a sapere che il web e i protocolli che ci permettono di navigare in rete sono nati grazie a standard aperti e software open source, e che Linux è - per le sue caratteristiche di versatibilità e affidabilità - il sistema operativo preferenziale per i centri di calcolo.
</p>
<p>
Linux è il sistema operativo dei server su cui girano tutti i grandi social network, e dei data center su cui si basano i servizi offerti da giganti come Google e Amazon. Inoltre, è stato il sistema operativo della seconda vittoriosa campagna elettorale di Barack Obama, coordinata da Harper Reed e giocata quasi interamente online sui social network e sui grandi siti di informazione.
</p>
<p>
Infine, Linux - e gli altri sistemi Unix-like - è presente anche nella maggior parte dei telefoni cellulari e dei tablet, e in modo meno visibile in quasi tutti i momenti della nostra vita, in quanto è il sistema operativo delle automobili, dei navigatori satellitari, delle TV, e di moltissimi elettrodomestici.
</p>
<p>
Nel 2012, il Linux Day ha visto la partecipazione di circa 15.000 persone in 119 sedi, con un migliaio di speaker che hanno parlato di distribuzioni GNU/Linux e di software per qualsiasi tipo di attività, dalla produttività individuale al gaming. Nel complesso, sono stati distribuiti 5.000 "Vademecum per il Software Libero".
</p>