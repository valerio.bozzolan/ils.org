<!--
.. title: La Libera Scuola
.. slug: la-libera-scuola
.. date: 2014-09-26 00:00:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
.. image_copy: 
.. previewimage:
-->

Nelle scorse settimane il Governo ha lanciato l'iniziativa <a rel="nofollow" href="https://labuonascuola.gov.it/">"La Buona Scuola"</a>, consultazione pubblica destinata a raccogliere spunti ed idee per migliorare il funzionamento ed i contenuti della scuola in Italia. <a href="/">Italian Linux Society</a> risponde a modo suo all'appello, approfittando dell'occasione per mettere nuovamente in evidenza l'esigenza di fornire alla didattica strumenti moderni ed efficaci pur tutelando la liberta' e la privacy di studenti ed insegnanti, l'autonomia delle istituzioni scolastiche, e anche le piu' che mai tormentate finanze.

Nasce "La Libera Scuola", iniziativa con cui intendiamo consigliare alcune soluzioni, software e non, esistenti e di alta qualita', ma soprattutto avanzare le nostre proposte e le nostre richieste al Ministero per l'Istruzione. Pochi e semplici spunti mirati a facilitare - o quantomeno non discriminare - l'utilizzo di software libero, e ad adottare un modello collaborativo e partecipato all'interno dell'intero sistema educativo (che coinvolga anche l'universita').

Dopo <a rel="nofollow" href="http://www.lastampa.it/2014/09/04/blogs/skuola/lim-su-non-le-usa-renzi-le-rottama-rdzmESmbR8fTVmXgUlysNL/pagina.html">le ammissioni</a> in merito all'infausta scelta delle Lavagne Interattive Multimediali, e premessi i <a rel="nofollow" href="http://www.programmailfuturo.it/">nuovi propositi</a> relativi all'insegnamento delle basi di informatica ai ragazzi (dunque: non l'uso passivo di Word ed Excel, ma gli algoritmi e la programmazione), speriamo che il Governo non perda nuovamente l'opportunita' di ascoltare e coinvolgere direttamente gli esperti del settore, coloro che da anni gia' percorrono la strada della consapevolezza tecnologica sui banchi di scuola.