<!--
.. title: Servizi.Linux.it
.. slug: servizi-linux-it
.. date: 2018-05-14 00:00:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
.. image_copy: 
.. previewimage:
-->

<p style="text-align:center">
    <img src="/images/posts/servizi.png" class="img-fluid">
</p>
<p>
A dispetto dei presupposti storici, tecnici e sociali di Internet oggi una mole sempre maggiore di servizi sono erogati da un numero sempre più ristretto di fornitori, accentrando utenti, dati e denaro nelle mani di pochi. A danno della pluralità, della privacy e dell'innovazione. Questi sono i presupposti della campagna "<a rel="nofollow" href="https://contributopia.org/en/home/">Contributopia</a>" lanciata da Framasoft, associazione francese che da anni si adopera per fornire applicazioni e piattaforme alternative (e, ovviamente, libere e opensource) da usare e far usare in opposizione a tale tendenza.
</p>
<p>
<a href="/">Italian Linux Society</a> aderisce oggi a tale iniziativa con <a href="http://servizi.linux.it/">servizi.linux.it</a>, presso cui è possibile fruire di una serie di servizi di utilità generale. L'intento della piattaforma è quello di offrire una scelta diversa ed un nuovo polo operativo in un'ottica di decentralizzazione, ma soprattutto di ispirare altri ad hostare servizi per amici, conoscenti, colleghi di lavoro e compagni di scuola su propri server, distaccati ed autonomi, al fine di distribuire oneri, costi, informazioni e contenuti sul maggior numero di istanze possibile.
</p>
<p>
Sul sito verranno man mano raccolti articoli e tutorial in italiano dedicati al tema della decentralizzazione online, e lo stesso servizi.linux.it è una istanza di <a rel="nofollow" href="https://sandstorm.io/">SandStorm</a>, progetto opensource mirato a semplificare la pubblicazione e la gestione indipendente di servizi web, che vi invitiamo ad esplorare, installare, ed arricchire di nuove applicazioni.
</p>