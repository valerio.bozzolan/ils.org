# Nikola on Docker
# Credits:
# https://gitlab.com/ItalianLinuxSociety/nikola-docker

FROM python:3.9-bookworm

# This will be read once by "pip install" and then removed.
COPY ./requirements.txt /tmp/requirements.txt

# Allow to build an image with different default UID/GID.
# This is probably not necessary for average users since Docker's --user=ID:GID
# should work at runtime for user mapping.
ARG IMAGE_UID=1000
ARG IMAGE_GID=1000

# Add dedicated user.
RUN groupadd --gid $IMAGE_GID dockola \
 && useradd  --uid $IMAGE_UID --gid $IMAGE_GID dockola --create-home \
 && mkdir --parents /app \
 && chown dockola:dockola   /app \
 && chown dockola:   /tmp/requirements.txt

WORKDIR /app

# From this moment, run as that dedicated low-privileged user.
USER dockola:dockola

RUN pip install --upgrade pip            --no-warn-script-location \
 && pip install -r /tmp/requirements.txt --no-warn-script-location \
 && rm             /tmp/requirements.txt

# Developers may want to run "nikola serve" and not just "nikola build".
EXPOSE 8000

# This is a generic wrapper for nikola serve and nikola build.
COPY ./entrypoint.sh /entrypoint.sh

# When the container is running should just serve the website.
# Note that developers may want to run just a "nikola build".
ENTRYPOINT [ "/entrypoint.sh" ]
