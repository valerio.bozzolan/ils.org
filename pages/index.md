.. title: Italian Linux Society
.. hidetitle: true
.. slug: index
.. description: L'associazione italiana per la promozione e la divulgazione di Linux e del Software Libero e Open Source

<div class="row mb-3">
    <div class="col-md-12 mb-5 text-center">
        <p class="display-4">
            L'associazione italiana per la promozione e la divulgazione di Linux e del Software Libero e Open Source
        </p>
    </div>
</div>

<div class="card text-white bg-secondary mb-5">
    <div class="card-body display-4">
        <img src="/images/linux_logo.png" alt="Tux, la mascotte Linux" class="float-right" loading="lazy" >
        <p>Vuoi saperne di più su Linux?</p>
        <a href="https://www.linux.it" class="stretched-link text-white">Scoprilo su www.linux.it!</a>
    </div>
</div>

<div class="card-deck mb-5 text-center">
    <div class="card border-dark">
        <div class="card-body text-dark">
            <p class="card-text">
                <a href="https://www.ils.org/iscrizione" class="stretched-link text-black">Iscriviti a ILS e aiutaci ad aiutare la community</a>
            </p>
        </div>
    </div>
    <div class="card border-dark">
        <div class="card-body text-dark">
            <p class="card-text">
                <a href="/sostieni" class="stretched-link text-black">Sostieni Italian Linux Society e contribuisci attivamente</a>
            </p>
        </div>
    </div>
    <div class="card border-dark">
        <div class="card-body text-dark">
            <p class="card-text">
                <a href="https://www.linux.it/stickers" class="stretched-link text-black">Richiedi gli stickers gratuiti, da distribuire a scuola e in ufficio</a>
            </p>
        </div>
    </div>
</div>

<div class="row">
    <div class="col">
        <a href="/news" class="btn btn-light float-right">Archivio News</a>
        <h2>Ultime News</h2>
    </div>
</div>
{{% post-list stop=4 %}}{{% /post-list %}}
